package view;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

import model.Player;

public class SpelFrame extends JFrame{
	private SpelPanel panel1;
	
	public SpelFrame(Player player){
		super();
		this.setSize( 900, 500 );
		this.setResizable(false);
		this.setLayout(null);
		panel1 = new SpelPanel(player);
		panel1.setBackground(Color.GRAY);
		panel1.setSize(new Dimension(800, 400));
		panel1.setLocation(25,50);
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.add(panel1);
	}
	
}