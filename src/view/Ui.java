package view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Player;

public class Ui {
	
	List<Player> players = new ArrayList<Player>();
	
	public void start() {
		registerPlayers();
		showFrames();
	}
	
	public void registerPlayers() {
		int a = 0;
		int aantal = Integer.parseInt(JOptionPane.showInputDialog("Welcome to Yathzee!\n\nWith how many players do you want to play?",2));
		while(a<aantal) {
			String name = JOptionPane.showInputDialog("What's your name?");
			Player p = new Player(name);
			players.add(p);
			a++;
		}
		
	}
	
	public void showFrames() {
		for (Player p : players) {
			SpelFrame frame = new SpelFrame(p);
			frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		    frame.setVisible( true );
		}
	}

}
