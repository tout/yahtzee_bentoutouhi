package view;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Player;

public class SpelPanel extends JPanel {
	
	public SpelPanel(Player player) {
		this.setLayout(new GridLayout(3, 1));
		this.add(new JLabel("Yahtzee"));
		this.add(new JLabel(player.getName() + " playing"));
	}

}
